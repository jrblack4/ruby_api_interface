require_relative 'api'

# PetStoreApi extends Api
class PetStoreApi < Api
  @base_uri = 'https://petstore.swagger.io/v2'

  @default_headers = {
    content_type: 'application/json',
    accept: 'application/json'
  }
end
